// get the modules
const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const cors = require('cors')
const multer = require('multer')


app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

//Representative State Transfer

// / route
app.get("/", (req, res) => {
    res.json("ping") //application/json
})


//here id is optional
// localhost: 8080 / 1
app.get("/:id?", (req, res) => {
    res.json({ ping: req.params.id })
})

// localhost: 8080 / pranav / 1
app.get("/:name?/:id?", (req, res) => {
    // res.json({ ping: req.params.id, ping1: req.params.name })
    res.json({ ping: req.params })
})




//query 
// localhost: 8080 / query ? name = pranav
app.post("/query", (req, res) => {
    res.json({ ping: req.query.name })
})


//parameter 
// localhost: 8080 / param / pranav / surname / kakadiya
app.post("/param/:name/surname/:surname", (req, res) => {
    res.json({ ping: req.params.name, ping1: req.params.surname })
})


//raw json
app.post("/body", (req, res) => {
    res.json({ ping: req.body })
})

//form-data
app.post("/formdata", multer().none(), (req, res) => {
    res.json({ ping: req.body })

    // // capture the encoded form data
    // req.on('data', (data) => {
    // console.log(data.toString());
    // });

    // // send a response when finished reading
    // // the encoded form data
    // req.on('end', () => {
    //     res.send('ok');
    // });
})


//header data
app.post('/headerdata', (req, res) => {
    // res.json({ ping: req.headers?.authorization });
    res.json({ ping: req.headers['content-type'] });
})


//header data in github
app.post('/form-urlencoded', (req, res) => {
    res.json({ formdata: req.body })
})


app.all("*", (req, res) => {
    res.json("not found")
})





//listing to the server on port 3000
app.listen(8080, () => {
    console.log('listing on port 8080')
})